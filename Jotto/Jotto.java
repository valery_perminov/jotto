
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Enumeration;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Jotto {
	public static void main(String[] args) {

		final JottoModel model = new JottoModel();

		JottoFrame jottoFrame = new JottoFrame("Jotto", new JottoSpringLayout(model));

		JMenuBar menuBar = new JMenuBar();
		jottoFrame.setJMenuBar(menuBar);

		JMenu menuSettings = new JMenu("Settings");
		menuBar.add(menuSettings);
		JMenuItem menuSettingsRestartGame = new JMenuItem("Restart Game");
		menuSettings.add(menuSettingsRestartGame);
		JMenuItem menuSettingsExit = new JMenuItem("Exit");
		menuSettings.add(menuSettingsExit);

		JMenu menuHelp = new JMenu("Help");
		menuBar.add(menuHelp);
		JMenuItem menuHelpInstructions = new JMenuItem("Instructions");
		menuHelp.add(menuHelpInstructions);

		menuSettingsRestartGame.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	model.restart();
		    }
		});

		menuSettingsExit.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	System.exit(0);
		    }
		});

		menuHelpInstructions.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
				"Instructions:\n\n"
				+"	- Upon launching the game, you will notice 3 panels.\n"
				+"	- The panel on the North-West side is the \"Settings\" panel.\n"
				+"	   - The \"Settings\" panel includes: difficulty level option, hints option, and target option\n"
				+"	   - The difficulty level option will determine the difficulty of the random chosen\n"
				+"	   target (\"Random Target Word\" button).\n"
				+"	   - Hints checkbox will enable hints\n"
				+"	   - To begin guessing the target, you are required to assign the target.\n"
				+"	   - To assign the target, you can either click the \"Random Target Word\" button or another\n"
				+"	   player can assign your target in the text field provided.\n"
				+"	   - Upon selecting your target, the \"Random Target Word\" button and the text field will lock.\n"
				+"	   If you wish to unlock the \"Random Target Word\" button and the text field, you must restart\n"
				+"	   the game. (To restart the game go to the menu -> select Options -> click \"Restart Game\")\n\n"
				+"	- The panel on the South-West side contains a text field and a table.\n"
				+"	   - You have to use the text field in order to guess the target.\n"
				+"	   - The table contains 3 columns: Words, Partial, and Exact. The \"Words\" column lists all of your\n"
				+"	   target guesses. The \"Partial\" column lists the number of partial matches that your guess has with\n"
				+"	   the target. The \"Exact\" column lists the number of exact matches that your guess has with the\n"
				+"	   target.\n\n"
				+"	- The large panel on the east has multiple panels with letters.\n"
				+"	   - If your hints are enabled, letter panels will turn green if the letter is a possible candidate\n"
				+"	   for the target or the letter panels  will turn red if the letter is not a possible candidate for\n"
				+"	   the target\n\n"
				+"	- I have also provided a menu bar which includes a \"Option\" and \"Help\" drop-downs.\n"
				+"	   - The \"Option\" drop-down includes an option to restart the game or exit the game.\n"
				+"	   - The \"Help\" drop-down includes a \"Instructions\" menu item which activates a pop-up with the\n"
				+"	   instructions.");

		    }
		   
		});
		jottoFrame.setVisible(true);
	}
}

class JottoFrame extends JFrame {

	private final JLabel label = new JLabel("message");

	private static int xPos = 10;
	private static int yPos = 10;
	private static final int OFFSET = 50;

	public JottoFrame(String title, JottoPanel contents) {
		super(title);
		this.setContentPane(contents);
		this.setSize(800, 650);
		this.setMinimumSize(new Dimension(750,650));
		this.getContentPane().setBackground(Color.blue);
		this.setLocation(xPos, yPos);
		xPos += OFFSET;
		yPos += OFFSET;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//this.setVisible(true);
	}


	/**
	 * Specify the feel -- how the widgets respond to input from the user.
	 * Note that Java uses the term "Listener" extensively.  In the more
	 * general UI lingo, they are "Controllers".
	 */
	private void registerControllers() {
		// Exit program instead of just hiding the window (default)
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});

	}


	class MenuItemListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JMenuItem mi = (JMenuItem)e.getSource();
			label.setText(mi.getText());
		}
	}

}

/**
 * Set attributes common to all panels.
 */
class JottoPanel extends JPanel {
	public JottoPanel() {
		super();
	}
}

class JottoSpringLayout extends JottoPanel {


	public JottoSpringLayout(JottoModel model) {
		super();
		SpringLayout springLayout = new SpringLayout();
		this.setLayout(springLayout);

		// Add constraints

		JottoView2 viewE = new JottoView2(model);
		springLayout.putConstraint(SpringLayout.NORTH, viewE, 10, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, viewE, 350, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.SOUTH, viewE, -10, SpringLayout.SOUTH, this);
		springLayout.putConstraint(SpringLayout.EAST, viewE, -10, SpringLayout.EAST, this);
		this.add(viewE);

		JottoView3 viewSW = new JottoView3(model);
		springLayout.putConstraint(SpringLayout.NORTH, viewSW, 265, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, viewSW, 10, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.SOUTH, viewSW, -10, SpringLayout.SOUTH, this);
		springLayout.putConstraint(SpringLayout.EAST, viewSW, -19, SpringLayout.WEST, viewE);
		this.add(viewSW);

		JottoView viewNW = new JottoView(model);
		springLayout.putConstraint(SpringLayout.NORTH, viewNW, 10, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, viewNW, 10, SpringLayout.WEST, this);
		springLayout.putConstraint(SpringLayout.SOUTH, viewNW, -18, SpringLayout.NORTH, viewSW);
		springLayout.putConstraint(SpringLayout.EAST, viewNW, 0, SpringLayout.EAST, viewSW);
		this.add(viewNW);

		// tell Model about View.
		model.addView(viewNW);
		model.addView(viewE);
		model.addView(viewSW);


	}

}
