

// HelloMVC: a simple MVC example
// the model is just a counter 
// inspired by code by Joseph Mack, http://www.austintek.com/mvc/

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.*;	

@SuppressWarnings("unchecked")
class JottoView extends JottoPanel implements IView {
	
	// the model that this view is showing
	private JottoModel model;

    private JLabel label2 = new JLabel("Difficulty:");
    private String[] field2List = {"Easy", "Midium", "Hard"};
    private JComboBox field2 = new JComboBox(field2List);
    private JLabel lblHint = new JLabel("Hints:");
    private JCheckBox chkHints = new JCheckBox("", false);
    
    private JLabel lbltarget = new JLabel("Target:");
    private JButton target = new JButton("Random Target Word");
    private JLabel lblOR = new JLabel("OR");
    private JLabel lbltarget2 = new JLabel("Target:");
    private JTextField txtTargetField = new JTextField(5);
    
    private JLabel lblTip = new JLabel("");
    
    private Boolean boolRandTarget = false;
	
	JottoView(JottoModel model_) {
		// create the view UI
        this.setBorder(BorderFactory.createTitledBorder("Settings "));
        SpringLayout layout = new SpringLayout();          
        this.setLayout(layout);
        
        layout.putConstraint(SpringLayout.WEST, label2, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, label2, 15, SpringLayout.NORTH, this);
		this.add(label2);
        
		layout.putConstraint(SpringLayout.NORTH, field2, 6, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, field2, 93, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, field2, -10, SpringLayout.EAST, this);
		this.add(field2);
		
        layout.putConstraint(SpringLayout.WEST, lblHint, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lblHint, 50, SpringLayout.NORTH, this);
		this.add(lblHint);
		
        layout.putConstraint(SpringLayout.WEST, chkHints, 90, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, chkHints, 48, SpringLayout.NORTH, this);
		this.add(chkHints);
		
        layout.putConstraint(SpringLayout.WEST, lbltarget, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lbltarget, 100, SpringLayout.NORTH, this);
        this.add(lbltarget);
        
		layout.putConstraint(SpringLayout.NORTH, target, 96, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, target, 93, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, target, -10, SpringLayout.EAST, this);
		this.add(target);
		
        layout.putConstraint(SpringLayout.WEST, lblOR, 170, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lblOR, 130, SpringLayout.NORTH, this);
        lblOR.setFont(new Font("Dialog", Font.BOLD, 16));
        this.add(lblOR);
		
        layout.putConstraint(SpringLayout.WEST, lbltarget2, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lbltarget2, 155, SpringLayout.NORTH, this);
        this.add(lbltarget2);
        
		layout.putConstraint(SpringLayout.NORTH, txtTargetField, 155, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, txtTargetField, 93, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, txtTargetField, -10, SpringLayout.EAST, this);
		this.add(txtTargetField);
		
        layout.putConstraint(SpringLayout.WEST, lblTip, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lblTip, 185, SpringLayout.NORTH, this);
        lblTip.setBackground(Color.blue);
        this.add(lblTip);
        
		// set the model 
		model = model_;
		
		model.setDifficulty("Easy", 0);
		
		field2.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	if(field2.getSelectedItem().toString().equalsIgnoreCase("Easy")){
		    		model.setDifficulty(field2.getSelectedItem().toString(),0);
		    	}else if(field2.getSelectedItem().toString().equalsIgnoreCase("Midium")){
		    		model.setDifficulty(field2.getSelectedItem().toString(),1);
		    	}else if(field2.getSelectedItem().toString().equalsIgnoreCase("Hard")){
		    		model.setDifficulty(field2.getSelectedItem().toString(),2);
		    	}
		    }
		});
		
		chkHints.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	if(chkHints.isSelected()){
		    		model.setHints(true);
		    	}else if(!chkHints.isSelected()){
		    		model.setHints(false);
		    	}
		    }
		});
	
		
		target.addActionListener (new ActionListener () {
		    public void actionPerformed(ActionEvent e) {
		    	if(boolRandTarget==false){
		    		target.setBackground(Color.GREEN);
		        	txtTargetField.setEditable(false);
		    		model.getTarget();
		        	model.setGameRunning();
		        	txtTargetField.setText("");
		        	boolRandTarget = true;
		        	target.setEnabled(false);
		        	lblTip.setText("<html><FONT COLOR=GREEN>*Target word has been set.</FONT");
		    	}
		    }
		});
		
		txtTargetField.getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {
				if(txtTargetField.getText().length()==5){
					if(model.inDictionary(txtTargetField.getText())){
			    		model.setTarget(new Word(txtTargetField.getText(),1));
			        	model.setGameRunning();
			        	txtTargetField.setEditable(false);
			        	target.setEnabled(false);
			        	lblTip.setText("<html><FONT COLOR=GREEN>*Target word has been set.</FONT");
			        	
						Runnable runnable = new Runnable() {
							@Override
							public void run(){
								txtTargetField.setText("*****");
								}
							};
						SwingUtilities.invokeLater(runnable);
			        	
					}else if(!txtTargetField.getText().equalsIgnoreCase("*****")){
						JOptionPane.showMessageDialog(null, "ERROR: Target does not meet requirements!");
						Runnable runnable = new Runnable() {
							@Override
							public void run(){
								txtTargetField.setText("");
								}
							};
						SwingUtilities.invokeLater(runnable);
					}
				}
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});	
	} 

	// IView interface 
	public void updateView() {
		System.out.println("View: updateView");
		if(model.isRestart()){
			field2.setSelectedIndex(0);
			chkHints.setSelected(false);
			
    		target.setBackground(null);
    		model.setTarget(null);
    		model.setGameStop();
    		txtTargetField.setEditable(true);
    		txtTargetField.setText("");
    		boolRandTarget = false;
        	target.setEnabled(true);
        	lblTip.setText("");
		}
	}
} 
