

// HelloMVC: a simple MVC example
// the model is just a counter 
// inspired by code by Joseph Mack, http://www.austintek.com/mvc/

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.*;

@SuppressWarnings("unchecked")
class JottoView2 extends JottoPanel implements IView {

	// the model that this view is showing
	private JottoModel model;

	private JottoPanel panels[] = { new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(),
			new JottoPanel(), new JottoPanel(), new JottoPanel(), new JottoPanel()};
	private JLabel labels[] = { new JLabel("A"), new JLabel("B"),
			new JLabel("C"), new JLabel("D"), new JLabel("E"), new JLabel("F"),
			new JLabel("G"), new JLabel("H"), new JLabel("I"), new JLabel("J"),
			new JLabel("K"), new JLabel("L"), new JLabel("M"), new JLabel("N"),
			new JLabel("O"), new JLabel("P"), new JLabel("Q"), new JLabel("R"),
			new JLabel("S"), new JLabel("T"), new JLabel("U"), new JLabel("V"),
			new JLabel("W"), new JLabel("X"), new JLabel("Y"), new JLabel("Z")};

	JottoView2(JottoModel model_) {
		// create the view UI
		GridLayout gridLayout = new GridLayout();
		this.setLayout(gridLayout);
		this.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		this.setLayout(new GridLayout(7, 4, 0, 0));

		for (int i = 0; i < 26; i++) {
			panels[i].setBorder(new TitledBorder(null, "", TitledBorder.LEADING,
					TitledBorder.TOP, null, null));
			this.add(panels[i]);
			SpringLayout panelLayout = new SpringLayout();
			panels[i].setLayout(panelLayout);

			panelLayout.putConstraint(SpringLayout.NORTH, labels[i], 10,
					SpringLayout.NORTH, panels[i]);
			panelLayout.putConstraint(SpringLayout.SOUTH, labels[i], -11,
					SpringLayout.SOUTH, panels[i]);
			panelLayout.putConstraint(SpringLayout.EAST, labels[i], -21,
					SpringLayout.EAST, panels[i]);
			panelLayout.putConstraint(SpringLayout.WEST, labels[i], 21,
					SpringLayout.WEST, panels[i]);
			labels[i].setHorizontalAlignment(panels[i].WIDTH / 2);
			labels[i].setFont(new Font("Dialog", Font.BOLD, 33));
			panels[i].add(labels[i]);
		}
	
		// set the model
		model = model_;
	}

	// IView interface
	public void updateView() {
		System.out.println("View: updateView");
		if(model.isGameRunning() && model.getJudgedWord()!="" && model.isHints() &&!model.isRestart()){
			if(model.getExact()==0 && model.getPartial()==0){
				for(int i = 0; i < model.getJudgedWord().length(); i++){
					for(int j = 0; j < labels.length; j++){
						if(labels[j].getText().equalsIgnoreCase(model.getJudgedWord().charAt(i)+"")){
							panels[j].setBackground(Color.RED);
						}
					}
				}
			}
			else if(model.getExact()>0 || model.getPartial()>0){
				for(int i = 0; i < model.getJudgedWord().length(); i++){
					for(int j = 0; j < labels.length; j++){
						if(labels[j].getText().equalsIgnoreCase(model.getJudgedWord().charAt(i)+"")){
							panels[j].setBackground(Color.GREEN);
						}
					}
				}
			}
		}else if(model.isRestart()){
			for(int i = 0 ; i < panels.length; i++){
				panels[i].setBackground(null);
			}
		}
	}
}
