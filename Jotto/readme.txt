Instructions:

WELCOME TO JOTTO!
- The point of the game is to accurately guess the target word.
- Please carefully read the instructions below to begin!

- Upon launching the game, you will notice 3 panels. 

- The panel on the North-West side is the "Settings" panel.
   - The "Settings" panel includes: difficulty level option, hints option, and target option
   - The difficulty level option will determine the difficulty of the random chosen 
   target ("Random Target Word" button).
   - Hints checkbox will enable hints
   - To begin guessing the target, you are required to assign the target.
   - To assign the target, you can either click the "Random Target Word" button or another
   player can assign your target in the text field provided.
   - Upon selecting your target, the "Random Target Word" button and the text field will lock.
   If you wish to unlock the "Random Target Word" button and the text field, you must restart
   the game. (To restart the game go to the menu -> select Options -> click "Restart Game")

- The panel on the South-West side contains a text field and a table.
   - You have to use the text field in order to guess the target. Upon entering a 5 letter word, it will be determined whether it is valid.
   - The table contains 3 columns: Words, Partial, and Exact. The "Words" column lists all of your
   target guesses. The "Partial" column lists the number of partial matches that your guess has with
   the target. The "Exact" column lists the number of exact matches that your guess has with the
   target.

- The large panel on the east has multiple panels with letters.
   - If your hints are enabled, letter panels will turn green if the letter is a possible candidate
   for the target or the letter panels  will turn red if the letter is not a possible candidate for 
   the target 

- I have also provided a menu bar which includes a "Option" and "Help" drop-downs.
   - The "Option" drop-down includes an option to restart the game or exit the game.
   - The "Help" drop-down includes a "Instructions" menu item which activates a pop-up with the
   instructions.
