


import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JOptionPane;


//View interface
interface IView {
	public void updateView();
}

public class JottoModel 
{
	public static int NUM_LETTERS = 5;
	public static final String[] LEVELS = {
      "Easy", "Medium", "Hard", "Any Difficulty"};
	private String sDifficulty = "";
	private int iDifficulty;
	
	// all views of this model
	private ArrayList<IView> views = new ArrayList<IView>();
	private Word target = null;
	private char[] arrTarget;
	private String judgedWord = "";
	private int exact = 0;
	private int partial = 0;
	
	private boolean start = false;
	private boolean hints = false;
	private boolean restart = false;
	
	//preprocessing
	WordList wl = new WordList("words.txt");

	// set the view observer
	public void addView(IView view) {
		views.add(view);
		// update the view to current state of the model
		notifyObservers();
	}
	
	public Boolean isGameRunning(){
		return start;
	}
	
	public void setGameRunning(){
		start = true;
	}
	
	public void setGameStop(){
		start = false;
	}
	
	public void setHints(Boolean b){
		hints = b;
	}
	
	public Boolean isHints(){
		return hints;
	}
	
	public Boolean isRestart(){
		return restart;
	}
	
	public void restart(){
		restart = true;
		notifyObservers();
		restart = false;
	}
	
	public Boolean inDictionary(String s){
	    if(wl.contains(s)){
	    	return true;
	    }
		return false;
		
	}
	
	public void judgeWord(String word){
		if(target == null){
			JOptionPane.showMessageDialog(null, "ERROR: Target word not found!");
		}else{
			judgedWord = word;
			String sTarget = target.getWord();
			char[] arrWord = word.toCharArray();
			arrTarget = target.getWord().toCharArray();
			exact = 0;
			partial = 0;
			if(word.equalsIgnoreCase(sTarget)){
				exact = 5;
				partial = 0;
			}else{
				for(int i = 0 ; i < 5; i++){
					if(sTarget.charAt(i)==word.charAt(i)){
						exact++;
						arrTarget[i] = '@';
						arrWord[i] = '!';
					}
				}
			
				for(int i = 0 ; i < arrTarget.length; i++){
					for(int j = 0; j < arrWord.length; j++){
						if(i!=j && arrTarget[i] == arrWord[j]){
							partial++;
							arrTarget[i] = '@';
							arrWord[j] = '!';
						}
					}
				}
			}
			notifyObservers();
		}
	}
	public String getJudgedWord(){
		return judgedWord;
	}
	
	public void getTarget(){
		target = wl.randomWord(iDifficulty);
		arrTarget = target.getWord().toCharArray();
	}
	
	public void setTarget(Word w){
		target = w;
		if(w!=null){
			arrTarget = target.getWord().toCharArray();
		}
	}
	
	public void setDifficulty(String d, int i){
		sDifficulty = d;
	}
	
	public int getExact(){
		return exact;
	}
	
	public int getPartial(){
		return partial;
	}
	
	// notify the IView observer
	private void notifyObservers() {
			for (IView view : this.views) {
				System.out.println("Model: notify View");
				view.updateView();
			}
	}
}
