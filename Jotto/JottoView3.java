

// HelloMVC: a simple MVC example
// the model is just a counter 
// inspired by code by Joseph Mack, http://www.austintek.com/mvc/

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.*;	
import java.util.Vector;

@SuppressWarnings("unchecked")
class JottoView3 extends JottoPanel implements IView {
	
	// the model that this view is showing
	private JottoModel model;
	private DefaultListModel<String> words = new DefaultListModel();
	private DefaultListModel<String> partial = new DefaultListModel();
	private DefaultListModel<String> exact = new DefaultListModel();
    private JLabel lblWord = new JLabel("Guess Target:");
    private JTextField field1 = new JTextField(5);
	JTable table = new JTable(new DefaultTableModel(new Object[]{"Word", "Partial","Exact"}, 1));
	JScrollPane scrollPane = new JScrollPane(table);
	DefaultTableModel tableModel = new DefaultTableModel(
				new Object[][] {},
				new String[] {
					"Words", "Partial", "Exact"
				}
			);
    
	JottoView3(JottoModel model_) {
		// create the view UI
        
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		
		table.setModel(tableModel);
		layout.putConstraint(SpringLayout.NORTH, scrollPane, 60, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, scrollPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, scrollPane, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, scrollPane, -5, SpringLayout.SOUTH, this);
		this.add(scrollPane);
		
		
		layout.putConstraint(SpringLayout.NORTH, lblWord, 20, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, lblWord, 15, SpringLayout.WEST, this);
		this.add(lblWord);
		
		field1 = new JTextField();
		layout.putConstraint(SpringLayout.NORTH, field1, 0, SpringLayout.NORTH, lblWord);
		layout.putConstraint(SpringLayout.EAST, field1, -80, SpringLayout.EAST, this);
		this.add(field1);
		field1.setColumns(10);

		// set the model 
		model = model_;
		
		// setup the event to go to the "controller"
		// (this anonymous class is essentially the controller)
		field1.getDocument().addDocumentListener(new DocumentListener() {
			public void insertUpdate(DocumentEvent e) {
				if(field1.getText().length()==5){
					if(model.inDictionary(field1.getText())){
						model.judgeWord(field1.getText());
						
						
						Runnable runnable = new Runnable() {
							@Override
							public void run(){
								field1.setText("");
							}
						};
						SwingUtilities.invokeLater(runnable);
					}
					else{
						JOptionPane.showMessageDialog(null, "ERROR: Word does not meet requirements!");
						Runnable runnable = new Runnable() {
							@Override
							public void run(){
								field1.setText("");
								}
							};
						SwingUtilities.invokeLater(runnable);
						}
				}
			}

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});	
	} 

	// IView interface 
	public void updateView() {
		System.out.println("View: updateView");
		if(model.isGameRunning() && !model.isRestart()){
			Vector rowData = new Vector(1);
			rowData.add(field1.getText());
			rowData.add(model.getPartial()+"");
			rowData.add(model.getExact()+"");
			tableModel.addRow(rowData);
			if(table.getRowCount()==10){
				JOptionPane.showMessageDialog(null, "You reached your maximum number of guesses! Play again?");
				model.restart();
			}else if(model.getExact()==5){
			        JOptionPane.showMessageDialog(null, "YOU WIN! Play again?");
                                model.restart();
			}
		}else if(model.isRestart()){
			tableModel.setRowCount(0);
		}
	}
} 
